#include <iostream>
#include <time.h>

using namespace std;
/*
void numera_espacios (int & matriz[n][n],int c, int d, int a, int b,int n)
  {
    if(matriz[c-1][d]!=0)
    {
      numera_espacios(matriz[n][n],c-1,d,a,b,n)
    }
    if(matriz[c+1][d]!=0)
    {
      numera_espacios(matriz[n][n],c+1,d,a,b,n)
    }
    if(matriz[c][d-1]!=0)
    {
      numera_espacios(matriz[n][n],c,d-1,a,b,n)
    }
    if(matriz[c][d+1]!=0)
    {
      numera_espacios(matriz[n][n],c,d+1,a,b,n)
    }

  }
*/

int main(){

  int srand(time(NULL));
  int n,r1,r2,a,b,c,d;
  cout<<"Digite el tamaño del cuadro: ";
  cin>>n;
  cout<<"Digite la posición inicial: "<<endl; cin>>a>>b;
  cout<<"Digite la posición final: "<<endl; cin>>c>>d;
  int matriz[n][n], suma_h[n],suma_v[n],suma_izq[n+2],suma_der[n+2];

  //crea números en la matriz.
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){  
    
      matriz[i][j] = 1;
    }   
  }

  //Crea el marco de la matriz.
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      matriz[0][j]=0;
      matriz[i][0]=0;
      matriz[n-1][j]=0;
      matriz[i][n-1]=0;
    }
  }

  //Crea los obstaculos.

  for(int i=0; i<n/2; i++){
    for(int j=0; j<n/2; j++){
      r1 = 1 + rand()%(n-2);
      r2 = 1 + rand()%(n-2);
      matriz[r1][r2]=0;
    }
  }

  //Posición inicial y final.

  for(int i=0; i<n; i++){
      if(a!=0 && b!=i){
        matriz[a][b]=9;
      }
      else if(a!=i && b!=0){
        matriz[a][b]=9;
      }
      else if(a!=n-1 && b!=i){
        matriz[a][b]=9;
      }
      else if(a!=i && b!=n-1){
        matriz[a][b]=9;
      }
      else{
        cout<<"Error."<<endl;
        break;
      }
  }
  for(int i=0; i<n; i++){
      if(c!=0 && d!=i){
        matriz[c][d]=8;
      }
      else if(c!=i && d!=0){
        matriz[c][d]=8;
      }
      else if(c!=n-1 && d!=i){
        matriz[c][d]=8;
      }
      else if(c!=i && d!=n-1){
        matriz[c][d]=8;
      }
      else{
        cout<<"Error."<<endl;
        break;
      }
  }

  

  /*Marca el recorrido.
  for(int k=0; k<n+2; k++){
    for(int i=k; i>(-1);i--){
      for(int j=0;j<k;j++){
        suma_izq[k]=0;
        suma_izq[k] += matriz[i][j];
        cout<<suma_izq[k]<<endl;
      }
    }
  } 
  */
  //Recorrido en la matriz.

 /* for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      if(matriz[a][b+j]==1){
        matriz[a][b+j]=7;
        if(matriz[][]){
           
        }
      }

    }
  }
*/

  //imprime la matriz.
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      cout<<matriz[i][j];
    }
    cout<<endl;
  }

  //imprime el cuadro.
  for(int i=0; i<n; i++){
    for(int j=0; j<n; j++){
      if(matriz[i][j]==0){
        cout<<"■ ";
      }
      else if(matriz[i][j]==9){
        cout<<"A ";
      }
       else if(matriz[i][j]==8){
        cout<<"B ";
      }
      else if(matriz[i][j]==1){
        cout<<"  ";
      }
    }
    cout<<endl;
  }

  return 0;
}