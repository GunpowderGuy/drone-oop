#include<iostream>
#include<vector>
#include<numeric>
#include<random>

using namespace std;

typedef vector<vector<int>> matriz;

int mirandom(int maximo = 2 ){
    random_device rd;
    static mt19937 g(rd());
    return uniform_int_distribution<int>(0,maximo-1)(g);
}

void fill_distances(matriz &m, int x, int y, int distancia){
    bool outsidebounds = (y >= m.size() || x >= m.size() || y < 0 || x < 0);

    if(!outsidebounds){
        bool incorrect_element = (m.at(x).at(y) == 0);
        if(  !incorrect_element){
            m.at(x).at(y) = distancia;

            fill_distances(m, x + 1, y, distancia + 1);
            fill_distances(m, x, y + 1, distancia + 1);
            fill_distances(m, x - 1, y, distancia + 1);
            fill_distances(m, x, y - 1, distancia + 1);
        }
    }
}

matriz distance_map(matriz m,int objetivo_x = 1,int objetivo_y = 1){
    fill_distances(m, objetivo_x, objetivo_y, 1);
    return m;
}

matriz generate_m(int columnas = 4,int filas = 4){
    vector<int> ej(filas);
    std::iota(ej.begin(), ej.end(), 0);

    vector<vector<int>> acum (columnas,ej);

    return acum;
}

string imprimible(matriz m){
    string acum;
    for(vector<int> mivector: m){
        for(int indice : mivector){
            acum += to_string(indice);//indice;
        }
        acum += "\n";
    }
    return acum;
}

int main(){
    cout << imprimible(distance_map(generate_m())).size();
    //cout << imprimible(generate_m());
    //cout << mirandom(2);cout << mirandom(2);cout << mirandom(3);
}